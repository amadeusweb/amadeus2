  </main><!-- End #main -->

<?php function rich_footer() {
	$extract = cs_var('rich_footer');
	if (!$extract) return false;
	extract($extract);
	include_once '_rich-footer.php';
	return true;
}

if (!rich_footer()) {?>
<hr />Copyright <?php echo date('Y') . ' - ' . cs_var('safeName'); }?>

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo $theme; ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo $theme; ?>assets/js/main.js"></script>

</body>

</html>
