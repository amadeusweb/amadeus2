  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-8 col-md-6 footer-contact"><?php if (cs_var('logo')) echo sprintf('<a href="./"><img src="%slogo-%s.png" alt="%s" class="img-fluid" /></a><br /><br />', cs_var('url'), cs_var('safeName'), cs_var('safeName')); ?>
            <!-- <h3><?php echo $title; ?></h3> -->
            <p class="description"><em><?php echo $description; ?></em></p>
            <p class="contact-details">
              <?php echo $address; ?><br>
              <strong>Phone:</strong> <a href="tel://<?php echo $phone; ?>"><?php echo $phone; ?></a><br>
              <strong>WhatsApp:</strong> <a href="https://wa.me/<?php echo $whatsapp; ?>" target="_blank"><?php echo $whatsapp; ?></a><br>
              <strong>Email:</strong> <a href="mailto:<?php echo $email . (isset($email_subject) ? $email_subject : ''); ?>"><?php echo $email; ?></a><br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul><?php foreach ($links as $text => $url) { ?>
              <li><a href="<?php echo $url; ?>"><?php echo $text; ?></a></li><?php } ?>
            </ul>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Our Initiatives</h4>
            <ul><?php foreach ($links2 as $text => $url) { ?>
              <li><a href="<?php echo $url; ?>"><?php echo $text; ?></a></li><?php } ?>
            </ul>
          </div>

<?php if (isset($facebook_widget)) { ?>
          <div class="col-lg-6 col-md-6 footer-facebook">
            <?php facebook_widget($facebook_widget);?>
          </div><?php } ?>

<?php if (isset($newsletter)) { ?>
          <div class="col-lg-6 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div><?php } ?>

        </div>
      </div>
    </div>

    <div class="container d-lg-flex py-4 footer-bottom">

      <div class="mr-lg-auto text-center text-lg-left">
        <div class="copyright">
          &copy; Copyright <strong><span><?php echo $title; ?></span></strong>. <?php echo (isset($start_year) ? $start_year . ' - ' : '') . date('Y'); ?> All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/ -->
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
      <?php if (isset($social)) {?>
      <div class="social-links text-center text-lg-right pt-3 pt-lg-0"><?php foreach ($social as $link) { ?>
        <a href="<?php echo $link['link']; ?>" target="_blank" class="<?php echo $link['name']; ?>"><i class="bx bxl-<?php echo $link['name']; ?>"></i></a>
<?php } ?>
      </div><?php } ?>
    </div>
  </footer><!-- End Footer -->
