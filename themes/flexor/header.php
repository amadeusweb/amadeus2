<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?php echo (cs_var('node') != 'index' ? cs_var('node') . ' - ' : '') . cs_var('name');?></title>
<?php seo_tags(); ?>

  <!-- Favicons -->
  <link href="<?php echo cs_var('url'); ?>icon-<?php echo cs_var('safeName'); ?>.png" rel="icon">
  <link href="<?php echo cs_var('url'); ?>apple-icon-<?php echo cs_var('safeName'); ?>.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo $theme; ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo $theme; ?>assets/css/style.css" rel="stylesheet">
<?php
if (cs_var('styles')) foreach (cs_var('styles') as $file)
  echo sprintf('    <link href="%sassets/%s.css" rel="stylesheet">
', cs_var('url'), $file);
if (cs_var('head_hooks')) foreach (cs_var('head_hooks') as $hook) include_once $hook;
?>
  <!-- =======================================================
  * Template Name: Flexor - v2.1.1
  * Template URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body class="<?php echo 'fol' . cs_var('safeFolder'); ?> page-<?php echo cs_var('node'); ?>">

<?php if (!cs_var('hide_topbar')) { ?>
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <ul>
          <li><i class="icofont-envelope"></i><a href="mailto:<?php echo cs_var('email'); ?>"><?php echo cs_var('email'); ?></a></li>
          <li><i class="icofont-phone"></i> <a href="tel://<?php echo cs_var('phone'); ?>"><?php echo cs_var('phone'); ?></a></li>
          <li><i class="icofont-phone"></i> <a href="tel://<?php echo cs_var('whatsapp'); ?>">WA: <?php echo cs_var('whatsapp'); ?></a></li>
          <li><i class="icofont-clock-time icofont-flip-horizontal"></i> <?php echo cs_var('timings'); ?></li>
        </ul>

      </div>
      <div class="cta">
        <?php echo cs_var('cta'); ?>
      </div>
    </div>
  </section><?php } ?>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <!-- <h1 class="text-light"><a href="<?php echo cs_var('url');?>"><span><?php echo cs_var('name'); ?></span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="<?php echo cs_var('url');?>"><img src="<?php echo cs_var('url'); ?>logo-<?php echo cs_var('safeName') . cs_var('safeFolder'); ?>.png" alt="<?php echo cs_var('name'); ?>" class="img-fluid"></a>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <?php if (isset($_GET['lorem'])) include "menu.html"; else menu(); ?>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
