    </main>
    
    <footer class="footer footer-1">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-7">
        <span class="hidden">Footer Links</span>			<div class="textwidget"><nav class="footer-nav" aria-label="footer-nav-other-areas">
  <a id="footer_nav_link" href="/locations/" title="LOCATIONS">LOCATIONS</a>
  <a href="/retail/" title="RETAIL">RETAIL</a>
  <a href="/contact-us/" title="CONTACT US">CONTACT US</a>
  <a href="/policies/" title="POLICIES">POLICIES</a>
  <a href="/press/" title="PRESS INQUIRIES">PRESS INQUIRIES</a>
</nav>

<nav class="footer-nav">
  <a href="/about/our-history/" title="ABOUT SSY">ABOUT SSY</a><a href="/careers/" title="CAREERS">CAREERS</a>
  <a href="https://ir.SSY.com/" title="INVESTOR RELATIONS">INVESTOR RELATIONS</a>
  <a href="/partnerships/" title="SELL YOUR STUDIO">SELL YOUR STUDIO</a>
  <a href="/ada-compliance-notice/" title="ADA NOTICE">ADA NOTICE</a>
</nav></div>
		      </div>
      <div class="col-sm-5 subscribe-wrap">
        			<div class="textwidget"><div class="cta-desc">
<a href="/gift-cards/?footer-promo" target="_blank" title="gift" rel="noopener noreferrer"><i class="icon icon-gift"></i>Gift Cards</a><a href="/refer-a-friend/?footer-promo" target="_blank" title="gift" rel="noopener noreferrer"><i class="icon icon-people"></i>Refer a Friend</a>
</div></div>
		      </div>
    </div>

    <div class="row subscribe-wrap">
      <div class="col-sm-6 col-md-4 right">
        <span class="hidden">Email Subscription</span><div class="textwidget custom-html-widget"><style type="text/css" media="screen">
    /* RESET */
    .elq-form * {
        margin: 0;
        padding: 0;
    }

    .elq-form h2 {
        margin-bottom: 7px;
        font-size: 16px;
        color: #333 !important;
        letter-spacing: 1px !important;
        font-weight: 400 !important;
    }

    .elq-form input, textarea {
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        box-sizing: content-box;
    }

    .elq-form input[type=text], .elq-form textarea, .elq-form select[multiple=multiple] {
        border: 1px solid #A6A6A6;
    }

    .elq-form button, input[type=reset], input[type=button], input[type=submit], input[type=checkbox], input[type=radio], select {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    /* GENERIC */
    .elq-form input {
        height: 16px;
        line-height: 16px;
    }

    .elq-form .item-padding {
        padding: 6px 5px 9px 9px;
    }

    .elq-form .field-wrapper.individual {
        float: left;
        width: 100%;
        clear: both;
    }

    .elq-form .field-p {
        position: relative;
        margin: 0;
        padding: 0;
    }

    /* FIELD GROUP */

    .elq-form .field-group .field-style {
        float: left;
    }

    #field0 {
        padding: 10px;
        width: 96%;
    }

    /* SIZING */
    .elq-form .field-style {
        margin-right: 2%;
        margin-left: 2%;
        height: 70px;
    }

    .elq-form .field-style._100 {
        width: 96%;
    }
    .elq-form .field-size-top-large {
        width: 100%;
    }

    /* POSITIONING */
    .elq-form .label-position.top {
        display: block;
        line-height: 150%;
        padding: 1px 0pt 3px;
        white-space: normal;
    }

    /* GRID STYLE */
    .elq-form .grid-style._100 {
        width: 96%;
    }
</style>
<div>
   <!--  <form method="post" name="SSYFooter" action="https://s126529962.t.eloqua.com/e/f2" onsubmit="return handleFormSubmit(this)" id="form38" class="elq-form">
        <input type="hidden" name="elqFormName" value="form38">
        <input type="hidden" name="elqSiteID" value="126529962">

        <div id="formElement0" class="sc-view form-design-field sc-static-layout item-padding sc-regular-size">
            <div class="field-wrapper">
            </div>
            <div class="individual field-wrapper">
                <div class="_100 field-style">
                    <h2>Join Our Community</h2>
                    <p class="field-p">
                        <input id="field0" name="emailAddress" type="text" value="" class="field-size-top-large" aria-label="Email Address">
                    </p>
                </div>
            </div>
        </div>
        <div id="formElement1" class="sc-view form-design-field sc-static-layout item-padding sc-regular-size">
            <div class="field-wrapper">
            </div>
            <div class="individual field-wrapper">
                <div class="_100 field-style">
                    <p class="field-p">
                        <input type="submit" value="Submit" class="submit-button btn-black" style="font-size: 100%; height: 40px; width: 150px">
                    </p>
                </div>
            </div>
        </div>
    </form> -->
    <script src="<?php echo $theme; ?>assets/livevalidation_standalone.compressed.js.download" type="text/javascript">
    </script>
    <style type="text/css" media="screen">
        .elq-form .loader {
            vertical-align: middle;
            display: inline-block;
            margin-left: 10px;
            border: 3px solid #f3f3f3;
            border-radius: 50%;
            border-top: 3px solid #3498db;
            width: 20px;
            height: 20px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .LV_validation_message {
            font-weight: bold;
            margin: 0 0 0 5px;
        }

        .LV_valid {
            color: #00CC00;
            display: none;
        }

        .LV_invalid {
            color: #CC0000;
            font-size: 10px;
        }

        .LV_valid_field, input.LV_valid_field:hover, input.LV_valid_field:active, textarea.LV_valid_field:hover, textarea.LV_valid_field:active {
            outline: 1px solid #00CC00;
        }

        .LV_invalid_field, input.LV_invalid_field:hover, input.LV_invalid_field:active, textarea.LV_invalid_field:hover, textarea.LV_invalid_field:active {
            outline: 1px solid #CC0000;
        }
    </style>
    <script type="text/javascript">
        var dom0 = document.querySelector('#form38 #field0');
        var field0 = new LiveValidation(dom0, {
                validMessage: "", onlyOnBlur: false, wait: 300
            }
        );

        function handleFormSubmit(ele) {
            var submitButton = ele.querySelector('input[type=submit]');
            var spinner = document.createElement('span');
            spinner.setAttribute('class', 'loader');
            submitButton.setAttribute('disabled', true);
            submitButton.style.cursor = 'wait';
            submitButton.parentNode.appendChild(spinner);
            return true;
        }

        function resetSubmitButton(e) {
            var submitButtons = e.target.form.getElementsByClassName('submit-button');
            for (var i = 0; i < submitButtons.length; i++) {
                submitButtons[i].disabled = false;
            }
        }

        function addChangeHandler(elements) {
            for (var i = 0; i < elements.length; i++) {
                elements[i].addEventListener('change', resetSubmitButton);
            }
        }

        var form = document.getElementById('form38');
        addChangeHandler(form.getElementsByTagName('input'));
        addChangeHandler(form.getElementsByTagName('select'));
        addChangeHandler(form.getElementsByTagName('textarea'));
        var nodes = document.querySelectorAll('#form38 input[data-subscription]');
        if (nodes) {
            for (i = 0, len = nodes.length; i < len; i++) {
                var status = nodes[i].dataset ? nodes[i].dataset.subscription : nodes[i].getAttribute('data-subscription');
                if (status === 'true') {
                    nodes[i].checked = true;
                }
            }
        }

        var nodes = document.querySelectorAll('#form38 select[data-value]');
        if (nodes) {
            for (var i = 0; i < nodes.length; i++) {
                var node = nodes[i];
                var selectedValue = node.dataset ? node.dataset.value : node.getAttribute('data-value');
                if (selectedValue) {
                    for (var j = 0; j < node.options.length; j++) {
                        if (node.options[j].value === selectedValue) {
                            node.options[j].selected = 'selected';
                            break;
                        }
                    }
                }
            }
        }
    </script>
</div></div>      </div>

      <div class="col-sm-6 col-md-8">
        <span class="hidden">Social Block</span>			<div class="textwidget"><div class="footer-desc">
        <p style="display: inline-block;"><br>&nbsp;<br></p>
</div>
<ul class="social-list">
      <li>
            <a href="https://www.facebook.com/SrisSchoolOfYogaFitness" target="_blank" title="facebook" class="icon icon-facebook" aria-label="SSY-facebook-page-icon" rel="noopener noreferrer"></a>
      </li>
      <!-- <li>
            <a href="https://twitter.com/SSY" target="_blank" title="twitter" class="icon icon-twitter" aria-label="SSY-twitter-page-icon"></a>
      
      <li>
            <a href="https://instagram.com/SSY" target="_blank" title="instagram" class="icon icon-instagram" aria-label="SSY-instagram-page-icon" rel="noopener noreferrer"></a>
      </li>
      <li>
            <a href="https://www.pinterest.com/SSY/" target="_blank" title="pinterest" class="icon icon-pinterest" aria-label="SSY-pinterest-page-icon" rel="noopener noreferrer"></a>
      </li>
      <li>
            <a href="https://www.youtube.com/user/SSY" target="_blank" title="youtube" class="icon icon-youtube" aria-label="SSY-youtube-page-icon" rel="noopener noreferrer"></a>
      </li>
      <li>
            <a href="https://www.linkedin.com/company/SSY" target="_blank" title="linkedin" class="icon icon-linkedin" aria-label="SSY-linkedin-page-icon" rel="noopener noreferrer"></a>
      </li>
       <li>
            <a href="http://accessible360.com/" aria-label="SSY-accessible-360-page-icon">
              <img src="assets/accessible360_under_review.png" style="height:75px; width:75px; margin-top: 15px;" alt="Under Review by Accessible360">
            </a>
      </li> -->
</ul></div>
		      </div>
    </div>
  </div>
  <div class="sub-footer">
      <span class="hidden">Sub Footer</span>			<div class="textwidget"><span>© 2020 SSY. ALL RIGHTS RESERVED.</span>
<nav class="sub-footer-nav" aria-label="footer-nav-terms-privacy">
      <a href="/terms-of-use" title="TERMS OF USE">TERMS OF USE</a>
      <a href="/privacy-policy" title="PRIVACY POLICY">PRIVACY POLICY</a>
</nav></div>
		  </div>
</footer>
	<style>
		.cmplz-blocked-content-container.recaptcha-invisible,
		.cmplz-blocked-content-container.g-recaptcha {
			max-width: initial !important;
			height: 70px !important
		}

		@media only screen and (max-width: 400px) {
			.cmplz-blocked-content-container.recaptcha-invisible,
			.cmplz-blocked-content-container.g-recaptcha {
				height: 100px !important
			}
		}

		.cmplz-blocked-content-container.recaptcha-invisible .cmplz-blocked-content-notice,
		.cmplz-blocked-content-container.g-recaptcha .cmplz-blocked-content-notice {
			top: 2px
		}
	</style>
		<style>
		.twitter-tweet.cmplz-blocked-content-container {
			padding: 10px 40px;
		}
	</style>
		<style>
		.cmplz-placeholder-element > blockquote.fb-xfbml-parse-ignore {
			margin: 0 20px;
		}
	</style>
		<style>
		.instagram-media.cmplz-placeholder-element > div {
			max-width: 100%;
		}
	</style>
	
<script src="<?php echo $theme; ?>assets/main-7dc1e8b1dc.js"></script>

<script src="<?php echo $theme; ?>assets/cookieconfig.min.js.download"></script>
			<script type="text/javascript" class="cmplz-stats">
							</script>
						<script class="cmplz-native">
				function complianz_enable_cookies() {
					console.log("enabling cookies");
									}
			</script>

  <a href="./#body-container" title="Back To Top" class="back-to-top" data-back-to-top="">
    <em class="icon icon-arrow-top"></em>
    <p>
      <span>Back</span>
      <span>To Top</span>
    </p>
  </a>
  


<!-- Dynamic page generated in 2.456 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2020-05-15 08:35:05 -->

<!-- super cache -->
</body></html>