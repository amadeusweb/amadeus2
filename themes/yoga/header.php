<!DOCTYPE html>
<!-- saved from url=(0026)/ -->
<html class="js canvas flexbox fontface desktop landscape windows windowsnt windowsnt_0 64bit chrome chrome81 chrome81_0 webkit en-us" lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
    <title><?php echo (cs_var('node') != 'index' ? cs_var('node') . ' - ' : '') . cs_var('name') . (cs_var('node') == 'index' ? ' | ' . cs_var('byline') : '');?></title>
    <title>SSY | Yoga Works for Everybody</title>

    <!-- All In One SEO Pack 3.4.3ob_start_detected [-1,-1] -->
    <meta name="description" content="SSY - Yoga for EVERY body. Start your yoga practice today with the best yoga teachers, yoga classes, group and private yoga classes, workshops, and yoga teacher training. Yoga can change your day and a practice can change your life.">

    <!-- All In One SEO Pack -->
    <link rel="dns-prefetch" href="https://cloud.typography.com/">
    <link rel="stylesheet" href="<?php echo $theme; ?>assets/fonts.css">
    <link rel="stylesheet" href="<?php echo $theme; ?>assets/dashicons.min.css">
    <link rel="stylesheet" href="<?php echo $theme; ?>assets/main-318d454555.css">
    <link rel="stylesheet" href="<?php echo $theme; ?>assets/style.min.css">
    <link rel="stylesheet" href="<?php echo $theme; ?>assets/animations.css">


    <?php
    if (cs_var('styles')) foreach (cs_var('styles') as $file)
        echo sprintf('    <link href="%sassets/%s.css" rel="stylesheet">
', cs_var('url'), $file);
    ?>

    <script src="<?php echo $theme; ?>../default/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo $theme; ?>assets/magnific-popup.js"></script>
    <script src="<?php echo $theme; ?>assets/nf-popups.js"></script>
    <?php
        if (cs_var('scripts')) foreach (cs_var('scripts') as $file)
        echo sprintf('    <script src="%sassets/%s.js" type="text/javascript"></script>
        ', cs_var('url'), $file);
    ?>


    <style>
        .simplified-mode-blk .ui-datepicker table tbody tr td .ui-datepicker-current-day a,
        .simplified-mode-blk .ui-datepicker table tbody tr td.ui-datepicker-today a {
            background-color: #fff !important;
            color: inherit !important;
        }

        .simplified-mode-blk .ui-datepicker table tbody tr td a.ui-state-active {
            background-color: #77026b !important;
            color: #fff !important;
        }

        .class-schedule-block tr#754125 {
            display: none !important;
        }

        div.top-corner-promo {
            display: none;
        }

        /*FOR QUARANTINE MODE ONLY. REMOVES TOP RIGHT FREE WEEK*/
        div.product-promo-banner {
            display: none;
        }

        /*FOR QUARANTINE MODE ONLY. REMOVES TOP NEW STUDENT OFFER ON LOCATION PAGES*/
        div.offer-1 .text-5 {
            display: none;
        }

        body .panel-layout div.so-panel.widget {
            margin-bottom: 0 !important;
        }

        /* The li tag is  dangerous. It touches everything in a list. The reason we're using it is being there wasn't another way to alter the Membership Tabs, but it also touches everything else on the site in a list - for example: it also touches all itmes in both the top nav menu and the locations menu. */

        /* h2 was only added because of the title on the new student offer page. */

        /* The p a tag and the ".training-hour .container .content div a" tag controls the color all <a> links inside <p> tags. It is also dangerous as it touches many things. It has forced us to add "color: #fff" to the button classes since the text was overwritten.*/

        /* The ".menu-item a" controls all the links in the top (main) nav at once. So it's split from ".navigation and .sub-menu" so it only universally controls font family and colors." */
        /* .Navigation and .sub-menu are there so we can control the font weights/sizes of the top (main) nav and it's sub-menu respectively. */
        /* .Header controls the background of the top nav menu. */

        /* The p tag, body tag, span tag (used for random filler text in places like the Membership Tagss and "Back to Top"), .copy, .content, .heading, .headline-copy (used on membership benefits), and .padding-right (used for about section locations), text-5-white (used on the centered background image widget), and .detail-block (used to change the descriptions on all workshop detail pages) covers all of the regular text on the site.*/

        /* .Filter-block used on widgets and .form-control used in text boxes for widgets. */

        /* .Select-group .text-3-black controls the titles and their font size on the teacher training widget. */

        /* Td.no-match is is the message that appears on all widgets when there are no search results found.*/

        /* The .text-4-black-1 (unqiue to locations page), .text-4-black, .text-4-white, .text-3-black (appears above the title and is hardcoded in the widet - cannot be lowercase), .title-4 (the "Studio Amenities" section" of locations), label (found on the teacher widget and likely elsewhere),  header-region-tpl (the headings of the login/sign-up widget), .header-title (the sub-header font on the checkout page), and .schedule-wrapper h4 (covers the sub-text under "Find Your Studio" on the regional landing pages we made)  cover all of the sub headers. */

        /* .filter-wrap #location-blk .title controls the title of the "Locations" in the "Find a Class" widget. */

        /* The .title-1, title-2, .region-name (in the login/create an account widget), .item-name.product-info-name (the header font on the checkout page), .title-7 (the title of the widget called YW - Two Columns - Promo (pka Membership Start) on our new student landing page), and .schedule-wrapper h1 (covers the "Find Your Studio" h1 on the regional landing pages we made) covers all of the text h1's/titles. */

        /* .Product-type controls both the "Class Packages" and "Membership" headings on the pricing pages. .Product-name controls the individual names of all class packages and memberships. .Product-price controls the price. .Product-unit controls quantity. .Promo-tab-control ul li span controls the 'Membership Yearly Tag Subtext' font. */

        /* The .link-2 and .link-3 are the links found on the location page  (ex. top left back to locations) and as the "CTA text" on other widgets and probably elsewhere. a#blog_cta_link.link-2 is the link-2 for the blog and it only needs it's mobile styling edited. */

        /* .form-brands div controls the "Curriculum" coloring on the Techer Training widget. */

        /* .back-to-top p span controls the text of the "Back to Top" widget. */

        /* .esignature-login bg-green changes the background color of the login portion of the login/create an account widget*/

        /* .product-line .line-right .most-popular changes the css of the "most popular" callout on the product pages. */

        .plan-plans .plan-btn a {
            color: #fff;
        }

        /*This is to edit the linked font of the buttons.*/

        .filter-wrap div#time-blk {
            display: none !important;
        }

        /*This is to remove the class time filter from the widget block.*/

        .page-id-474182 main#main.wrapper.wrapper-1 {
            max-width: 1000px !important;
        }

        /*For cookie information page.*/

        .cc-compliance a.cc-btn.cc-dismiss {
            text-transform: uppercase !important;
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
        }

        /*For upper-casing and putting correct font on cookie button.*/

        a {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
        }

        li {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
        }

        h2 {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-weight: 200 !important;
        }

        /*found as title of new student offer page */

        p {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        body {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        span {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
        }

        .mbo-promo-text span.sub-title {
            font-family: 'Lato,'sans-serif !important;
            font-size: 12pt !important;
        }

        /*This is the "have a promo code?" copy on the checkout page */
        .copy {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        .content {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        .heading {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        .headline-copy {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 15px !important;
            line-height: 1.48 !important;
        }

        .padding-right {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        .text-5-white {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
            color: #333
                /*this used to be white*/
                 !important;
        }

        .detail-block {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        .filter-block {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            background-color: white !important;
        }

        .form-control {
            background-color: white !important;
        }

        .select-group .text-3-black {
            font-size: 10pt !important;
        }

        /*widget title sizes */

        td.no-match {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 16px !important;
            line-height: 1.48 !important;
        }

        .text-4-black-1 {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b;
             !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
            margin-top: 0.75em !important;
            margin-bottom: 0.25em !important;
        }

        .text-4-white {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b
                /*this used to be white*/
            ;
             !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
            margin-top: 0.75em !important;
            margin-bottom: 0.25em !important;
        }

        .text-4-black {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
        }

        /* cannot un-capitalize this one */
        .text-3-black {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
        }

        .title-4 {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
        }

        label {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            color: #333 !important;
            letter-spacing: 1px !important;
            font-weight: 400 !important;
        }

        .header-region-tpl {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
        }

        .header-title {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
        }

        .schedule-wrapper h4 {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 12pt !important;
            color: #77026b;
             !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: 700 !important;
            letter-spacing: 4px !important;
            margin-top: 1.875rem !important;
            margin-bottom: 0.25em !important;
        }

        a.text-4-black-1:hover {
            color: #5a8128 !important;
        }

        /*changes hover over on the location pages */


        .filter-wrap #location-blk .title {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            line-height: 1.25 !important;
            color: #77026b !important;
            font-weight: 700 !important;
            letter-spacing: 1px !important;
            text-transform: uppercase !important;
        }


        .link-2 {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            ;
            font-size: 14px !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: bold !important;
            letter-spacing: 2px !important;
        }
        }

        .link-3 {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            ;
            font-size: 14px !important;
            text-transform: uppercase !important;
            line-height: 1.25 !important;
            font-weight: bold !important;
            letter-spacing: 2px !important;
        }

        @media only screen and (max-width: 768px) {

            /* For Mobile on the Blog Link */
            a#blog_cta_link.link-2 {
                background-color: #d494ac !important;
                border-radius: 15px !important;
            }
        }

        /*the border radius and background color are here because the blog link turns into a black, square button on mobile. */


        @media only screen and (min-width: 769px) {
            /* For Desktop */

            .title-1 {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 42pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333 !important;
            }

            .title-2 {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 42pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333
                    /*this used to be white*/
                     !important;
                padding: 20px 0 !important;
            }

            .region-name {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 42pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333 !important;
            }

            .item-name.product-info-name {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 30pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333 !important;
            }

            .title-7 {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                padding-top: 5px !important;
                color: #333 !important;
            }

            .schedule-wrapper h1 {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 42pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333 !important;
            }

            .training-content .title-1 {
                padding-bottom: 20px !important;
            }

            /*this is the title of the teacher training widget with 2 CTAs*/
            .inner .title-1 {
                padding: 20px 0 !important;
            }
        }

        @media only screen and (max-width: 768px) {
            /* For Mobile */

            .title-1 {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 30pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333 !important;
            }

            .title-2 {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 30pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333
                    /*this used to be white*/
                     !important;
                padding: 20px 0 !important;
            }

            .region-name {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 30pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333 !important;
            }

            .item-name.product-info-name {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 30pt !important;
                font-weight: 200 !important;
                padding-bottom: 0px !important;
                color: #333 !important;
            }

            .training-content .title-1 {
                padding-bottom: 20px !important;
            }

            /*this is the title of the teacher training widget with 2 CTAs*/
            .inner .title-1 {
                padding: 20px 0 !important;
            }
        }


        @media only screen and (min-width: 769px) {

            /* For Desktop */
            .product-type {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 42pt !important;
                font-weight: 100 !important;
                padding-bottom: 15px !important;
                color: #333 !important;
            }
        }

        @media only screen and (max-width: 768px) {

            /* For Mobile */
            .product-type {
                font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
                font-size: 30pt !important;
                font-weight: 100 !important;
                padding-bottom: 15px !important;
                color: #333 !important;
            }
        }

        .product-name {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-weight: 200 !important;
            padding-bottom: 0px !important;
            color: #333 !important;
        }

        .product-price {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-weight: 300 !important;
        }

        .product-unit {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
        }

        .promo-tab-control ul li span {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
        }

        p.product-savings {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-size: 10pt !important;
            color: #747474 !important;
        }

        .btn-gold {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-weight: bold !important;
            border-radius: 15px !important;
            font-size: 10pt !important;
        }

        .btn-black {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-weight: bold !important;
            border-radius: 15px !important;
            font-size: 10pt !important;
        }

        .btn-green {
            font-family: "Gotham SSm A", "Gotham SSm B", sans-serif !important;
            font-weight: bold !important;
            border-radius: 15px !important;
            font-size: 10pt !important;
        }

        input#mbo-btn-login.btn.btn-block.btn-custom.btn-green.btn-sign-in {
            line-height: 0 !important;
            width: 200px !important;
        }

        /*login button on the login/create account widget */
        input.btn.btn-block.btn-custom.btn-black.btn-new-account {
            line-height: 0 !important;
            width: 200px !important;
        }

        /*new account registration button on the login/create account widget */

        .form-brands div {
            color: #333 !important;
        }

        .back-to-top p span {
            font-size: 8pt !important;
        }

        .product-line .line-right .most-popular {
            position: relative !important;
            top: -25px !important;
            z-index: -100 !important;
            padding-top: 28px !important;
            border-radius: 15px !important;
            background-color: #404040 !important;
        }

        /*This next section is all for the locations page ONLY. */
        .location-item .row .col-sm-6.col-md-4 a.link-img {
            display: none !important;
        }

        /*This removes the images on all of the locations pages.*/
        .location-item .location-head .title-icon-group .row .col-sm-4.col-md-3 .list-icon {
            display: none !important;
        }

        /* Removes the 'Getting There' text and icon. */
        .location-item .row .col-sm-6.col-md-4 .content .icon-group {
            display: none !important;
        }

        /*Removes driving directions icon on each title page */
        @media only screen and (min-width: 769px) {

            /* For Desktop */
            .location-item .row .col-sm-6.col-md-4 {
                text-align: left !important;
                background-color: rgba(23, 170, 157, .06) !important;
                margin-right: 20px !important;
                margin-top: 20px !important;
                width: 30% !important;
                padding-bottom: 15px !important;
            }

            /*This controls the divs containing each location on the main locations page. */
        }

        @media only screen and (max-width: 768px) {

            /* For Mobile */
            .location-item .row .col-sm-6.col-md-4 {
                text-align: left !important;
                background-color: rgba(23, 170, 157, .06) !important;
                margin-right: 20px !important;
                margin-top: 20px !important;
                width: 96% !important;
                padding-bottom: 15px !important;
            }

            /*This controls the divs containing each location on the main locations page. */
        }

        .location-head .title-icon-group {
            margin-bottom: 0px !important;
            padding-bottom: 0px !important;
            padding-top: 0px !important;
        }

        /*Helps with spacing but I have no idea how we found this.*/
        .location-item .row .col-sm-6.col-md-4 .content .desc p {
            font-size: 11pt !important;
        }

        /* This controls the text info inside each location box on the main locations page.*/
        .location-item {
            padding-left: 15px !important;
            margin-bottom: 3.125rem !important;
        }

        /*This controls the spacing between region names.*/
        /*end of locations page. */

        a#corner_cta_2_link.btn-register {
            display: none !important;
        }

        /* this is a wierd triangle part of one of the widget on the "home page" */
        a#hp_corner_cta_link.btn-register {
            display: none !important;
        }

        /* this is a wierd triangle part of one of the widget on the "find a class page" */

        a#left_tt_cta_link.btn-green span.big {
            padding-top: 25px !important;
        }

        /*this is the left button on the teacher training widget */
        a#right_tt_cta_link.btn-green span.big {
            padding-top: 25px !important;
        }

        /*this is the right buttons on the teacher training widget */
        a#tt_cta_link.link-2 {
            display: none !important;
        }

        /* this is an unnecessary link in the teacher training widget */

        section.training-form {
            background-color: #F5F5F5 !important;
        }

        /*changing background color on the teacher training contact form */

        em.icon.icon-logo {
            display: none !important;
        }

        /* this is a really small SSY logo that is appearing sporadically*/

        div.one-half.separate::before {
            display: none !important;
        }

        /*this is to remove the line separator before the Teacher Training Widget with 2 CTAs*/
        div.one-half.separate::after {
            display: none !important;
        }

        /*this is to remove the line separator before the Teacher Training Widget with 2 CTAs*/

        .training-classes-blk .teacher-bio img {
            height: 325px !important;
            width: 325px !important;
            max-width: none !important;
        }

        .class-location span {
            font-weight: 300 !important;
        }


        /*Please ignore this section for the true style guide. These are being used to edit specific widgets on specific pages with specific IDs. */
        a#title_home_w_cta_link.btn-black {
            background-color: #d494ac !important;
        }

        /* This is here to change the button color of the 'YW - Title with CTA link Block Widget .' that exists on the home page. PLEASE REVISIT THIS*/

        .page-id-190660 li[data-value="9-atlanta"] {
            display: none !important;
        }

        /*for a ninja form on some page. come back to detail.*/
        .page-id-190660 li[data-value="8-houston"] {
            display: none !important;
        }

        /*for a ninja form on some page. come back to detail.*/

        div#pl-197311.panel-layout .text-7 {
            display: none !important;
        }

        /*to remove empty p tags on the top of the widget called YW - Two Columns - Promo (pka Membership Start) on our new student landing page */
        div#pl-197311.panel-layout .text-8 {
            display: none !important;
        }

        /*to remove empty p tags on the top of the widget called YW - Two Columns - Promo (pka Membership Start) on our new student landing page */

        div#pgc-185125-0-0.panel-grid-cell .mbo-header {
            display: none !important;
        }

        /*this removes the SSY banner on the new student checkout page */

        /*.page-id-132075 header.header.header-1 {display: none !important;} removes top nav and top black bar from new student pages */

        .houston-rockets-only p {
            color: white !important;
        }

        .houston-rockets-only h2 {
            color: white !important;
        }

        .houston-rockets-only h3 {
            color: white !important;
        }

        .houston-rockets-only .btn-green {
            background-color: #C4DE9E !important;
        }

        @media only screen and (max-width: 768px) {
            .swipe p.text-4-black {
                letter-spacing: 1px !important;
                font-size: 6pt !important;
                white-space: normal !important;
            }
        }

        /* To make Mobile html sliders responsive*/


        .workshop-detail .container {
            margin: 0px !important;
            padding: 0px !important;
            width: 100% !important;
        }

        /*this is being used to slightly re-design all workshop pages */
        .workshop-detail .main-block {
            margin: 0px !important;
        }

        /*this is being used to slightly re-design all workshop pages */
        section.event-data-blk.workshop-detail.based-styles img.banner.hidden-xs {
            display: none !important
        }

        /*this is being used to slightly re-design all workshop pages */
        section.event-data-blk.workshop-detail.based-styles img.banner.visible.xs {
            display: none !important
        }

        /*this is being used to slightly re-design all workshop pages */
        @media only screen and (min-width: 768px) {
            .workshop-detail .main-block .detail-block {
                padding: 0px 120px 0px 120px !important;
            }

            /*this is being used to slightly re-design all workshop pages */
            .workshop-detail .main-block .banner-blk {
                background-image: url(https://dbafh6w2m82ux.cloudfront.net/app/uploads/2019/02/12133528/locations-hero21.jpg) !important;
                background-size: 100% 100% !important;
            }

            /*this is being used to slightly re-design all workshop pages */
            .workshop-detail .level.l-all {
                display: none;
            }

            /* removing "all level" tag */
        }

        @media only screen and (max-width: 768px) {
            .workshop-detail .main-block .detail-block {
                padding: 0px 0px 0px 0px !important;
            }

            /*this is being used to offset the new workshop design above when in mobile view */
            .workshop-detail .main-block .banner-blk {
                background-image: display: none !important;
                background-size: 100% 100% !important;
            }

            /*this is being used to offset the new workshop design above when in mobile view */
        }
    </style>
    <script type="text/javascript">
        setTimeout(function() {
            var a = document.createElement("script");
            var b = document.getElementsByTagName('script')[0];
            a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0031/8038.js";
            a.async = true;
            a.type = "text/javascript";
            b.parentNode.insertBefore(a, b)
        }, 1);
    </script>
    <style type="text/css" media="all" id="siteorigin-panels-layouts-head">
        /* Layout 117821 */
        #pgc-117821-0-0 {
            width: 100%;
            width: calc(100% - (0 * 30px))
        }

        #pl-117821 #panel-117821-0-0-0,
        #pl-117821 #panel-117821-0-0-1,
        #pl-117821 #panel-117821-0-0-2,
        #pl-117821 #panel-117821-0-0-3,
        #pl-117821 #panel-117821-0-0-4,
        #pl-117821 #panel-117821-0-0-5 {}

        #pl-117821 .so-panel {
            margin-bottom: 30px
        }

        #pl-117821 .so-panel:last-child {
            margin-bottom: 0px
        }

        @media (max-width:768px) {

            #pg-117821-0.panel-no-style,
            #pg-117821-0.panel-has-style>.panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-117821-0>.panel-grid-cell,
            #pg-117821-0>.panel-row-style>.panel-grid-cell {
                width: 100%;
                margin-right: 0
            }

            #pl-117821 .panel-grid-cell {
                padding: 0
            }

            #pl-117821 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-117821 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }
        }
    </style>
   
</head>

<body data-cmplz="1" id="body-container" class="home page siteorigin-panels siteorigin-panels-home cmplz-status-all">
    <div role="dialog" aria-live="polite" aria-label="cookieconsent" aria-describedby="cookieconsent:desc" class="cc-window cc-banner cc-type-opt-out cc-theme-classic cc-bottom cc-color-override-738676082  cc-invisible" style="display: none;">
        <!--googleoff: all--><span id="cookieconsent:desc" class="cc-message">Cookies are important to the proper functioning of a site. To improve your experience, we use cookies to remember log-in details and provide secure log-in, and collect statistics to optimize site functionality. <a aria-label="Learn More to Opt Out" tabindex="0" class="cc-link" href="/">Learn More to Opt Out</a></span>
        <div class="cc-compliance cc-highlight"><a aria-label="deny cookies" role="button" tabindex="0" class="cc-btn cc-deny"></a><a aria-label="Accept" href="/#" role="button" tabindex="0" class="cc-btn cc-dismiss">Accept</a></div>
        <!--googleon: all-->
    </div>

    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.      </div>
    <![endif]-->
    <a href="/#main" class="skip-link">Skip to main content</a>
    <header class="header header-1">
        <div class="sub-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="header-ticket" style="display:none">
                        <div class="header-inner" style="background-color:#77026b;">
                            <a data-promo-tag="" id="global_top_cta_link" data-promo-name="Top Banner" data-position="Global Top Banner" href="/classes/live/" title="&lt;strong&gt;CHOOSE FROM OVER 100 LIVE CLASSES + ACCESS OUR FREE COMMUNITY CLASSES&lt;/strong&gt;" class="link-1">
                                <strong>CHOOSE FROM OVER 100 LIVE CLASSES + ACCESS OUR FREE COMMUNITY CLASSES</strong> </a>
                            <span class="pull-right x-close hairline white" title="dismiss"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "menu-yw.php"; ?>
    </header>

    <main class="wrapper wrapper-1" id="main">