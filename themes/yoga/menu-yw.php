<div class="nav-wrapper" data-navigation="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 text-left">
                <a class="logo" href="./" title="SSY">
                    <img src="<?php echo cs_var('url'); ?>assets/logo.svg" alt="SSY Logo">
                </a>
            </div>
            <div class="col-md-10 navigation-wrapper">
                <button type="button" class="btn-transparent btn-nav-open" href="javascript:;" title="control" aria-label="Mobile Device Navigation" aria-expanded="false" aria-controls="yw-main-menu">
                    <em class="separate"></em>
                    <em class="separate"></em>
                    <em class="separate"></em>
                </button>

                <div class="navigation scrollable" id="yw-main-menu" role="navigation" aria-label="primary" style="display: none;">
                    <div class="inner">
                        <a href="https://www.SSY.com/#" title="Close" class="btn-close"></a>
                        <ul id="menu-main-menu" class="nav main-navigation">
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-488787" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-488787"><a title="NEW!  Menu Item" href="#" id="menu-item-a"><strong>
                                        <font color="#77026b">NEW!</font>
                                    </strong> Menu Item</a></li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-146" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-146 has-child dropdown"><a title="Menu Item I" href="#" data-toggle="dropdown" aria-haspopup="true" id="menu-item-i">Menu Item I</a>
                                <ul class="sub-menu">
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a title="In Studio" href="#" id="menu-in-studio">Sub Menu I-A</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-503389" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-503389"><a title="Online" href="#" id="menu-online">Sub Menu I-B</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a title="Workshops" href="#" id="menu-workshops">Sub Menu I-C</a></li>
                                </ul>
                            </li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-165" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-165 has-child dropdown"><a title="Menu Item" href="#" data-toggle="dropdown" aria-haspopup="true" id="menu-locations">Menu Item II </a>
                                <ul class="sub-menu">
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-137875" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-137875"><a title="Atlanta" href="#" id="menu-atlanta">Sub Menu II-A</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-13469" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13469"><a title="Baltimore" href="#" id="menu-baltimore">Sub Menu II-B</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-2007" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2007"><a title="Boston" href="#" id="menu-boston">Sub Menu II-C</a></li>
                                </ul>
                            </li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a title="Menu Item B" href="#" id="menu-item-b">Menu Item B</a></li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-40" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-40 has-child dropdown"><a title="Menu Item" href="#" data-toggle="dropdown" aria-haspopup="true" id="menu-teacher-training">Menu Item III</a>
                                <ul class="sub-menu">
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-293918" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-293918"><a title="200-Hour" href="#" id="menu-200-hour">Sub Menu III-A</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-293919" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-293919"><a title="Advanced 300-Hour" href="#" id="menu-advanced-300-hour">Sub Menu III-B</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-293920" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-293920"><a title="Contact Us" href="#" id="menu-contact-us">Sub Menu III-C</a></li>
                                </ul>
                            </li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1782" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1782 has-child dropdown"><a title="Menu Item" href="#" data-toggle="dropdown" aria-haspopup="true" id="menu-about">Menu Item IV</a>
                                <ul class="sub-menu">
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-331"><a title="Our History" href="#" id="menu-our-history">Sub Menu IV-A</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-326142" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-326142"><a title="Our Classes" href="#" id="menu-our-classes">Sub Menu IV-B</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-228" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-228"><a title="Our Teachers" href="#" id="menu-our-teachers">Sub Menu IV-C</a></li>
                                </ul>
                            </li>
                            <li class="menu-top-promo">
                                <a id="free-week" href="https://www.SSY.com/checkout/new-student/" class="cta-bg-color">
                                    Free Week </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="user-nav-wrapper">
                    <nav class="user-nav" aria-label="user">
                        <div class="login-wrapper" data-login-nav="">
                            <a href="https://www.SSY.com/#" class="login-control" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <em class="icon icon-login"></em>
                                <span>My Account</span>
                            </a>
                            <ul class="login-nav">
                                <li>
                                    <a href="#" target="_blank"><span>Account Sub Menu-I</span></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><span>Account Sub Menu-II</span></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>