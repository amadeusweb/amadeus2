
        <div id="pl-117821" class="panel-layout"><div id="pg-117821-0" class="panel-grid panel-no-style"><div id="pgc-117821-0-0" class="panel-grid-cell">        <section class="banner-blk">
            <a data-promo-tag="" id="hp_hero_img_link" data-promo-name="Homepage Hero Image" data-position="Homepage Hero" href="https://www.SSY.com/classes/live/" title="Join Us On SSY  Live">            <img src="<?php echo $theme; ?>assets/5.20-YWL-BANNERS-1.jpg" alt="Join Us On SSY  Live">
            </a>        </section>
            <section class="banner-blk">
            <div class="content text-center">
                <p class="text-3-black">FROM PRACTICE ROOM TO LIVING ROOM</p>
                                <h1 class="title-1">100+ Live Classes Every Day</h1>
                                <div>Hunkering down at home can be hard. That’s why we’re excited to bring our top teachers directly to you! Get ready to explore new teachers near and far as you continue your journey with us online, and the best part is our line up of live classes will be ever-expanding so you can easily find that perfect class to fit any mood.
<br><br>
Plus, with so many within our community affected by this pandemic in ways big and small, we are honored to offer a number of <font color="#77026b"><strong>daily free community classes</strong></font>, so no matter your current situation, you can continue to deepen your practice with us.</div>
                                                        <a data-promo-tag="" id="title_w_cta_link" data-promo-name="Homepage Hero CTA" data-position="Homepage Hero" href="https://www.SSY.com/classes/live/" class="btn btn-black">STREAM A LIVE CLASS</a>
	                                            </div>
        </section>
                <section class="register-blk">
            <div class="content">
                <h3 class="text-4-black">Our Studios Are Temporarily Closed</h3>
                <div class="side-margins">
                    <p>The health and well being of our teachers, staff and students, as well as that of our local communities, is always our number one priority, and for that reason we have to make the difficult decision to temporarily close our studios effective Tuesday, March 17th, until further notice.</p>                </div>
                                <a id="center_cta_link" href="https://www.SSY.com/covid-19-corona-virus/" title="New call to action 1 link title" class="link-2">READ FULL UPDATE</a>
                            </div>
                    </section>
                <section id="free-on-demand-yoga-with-love-widget" class="simple-blk block-1" data-set-height="">
            <div class="container-fluid">
                <div class="row">
                                    <div class="one-half text-left" style="height: 524.566px;">
                        <img src="<?php echo $theme; ?>assets/yw-healthcare-workers-myw-750x625-1.jpg" alt="For Our Healthcare Workers" class="img-block-1">
                    </div>
                                        <div class="one-half second-order" style="height: 524.566px;">
                        <div class="inner">
	                                                        <h2 class="title-1"><div class="text-3-black">Free On-demand Yoga, With Love</div></h2>
	                                                    <div class="copy"><font size="2">We're so grateful for every healthcare employee working tirelessly to fight this pandemic. With long shifts, little sleep, and stressful work environments, they are true heroes on the front lines of this fight. That's why we're so honored to donate access to our online yoga platform <a href="https://www.mySSY.com/"><strong>MySSY</strong></a> to health organizations and front line workers around the world, with a goal of helping to create a moment of calm in the heart of chaos.
<br><br>
Do you know someone in the healthcare industry who would like to provide free on-demand yoga to their colleagues? We'd love to get in touch! Email us at <a href="mailto:support@mySSY.com"><strong>support@mySSY.com</strong></a>, and we'd be happy to assist. 
</font></div>
                                                    </div>
                    </div>
                                        </div>
            </div>
        </section>
                <section id="find-a-class-widget" class="simple-blk block-1" data-set-height="">
            <div class="container-fluid">
                <div class="row">
                                    <div class="one-half second-order" style="height: 631.5px;">
                        <div class="inner">
	                                                        <h2 class="title-1">Find A Class</h2>
	                                                    <div class="copy">We offer yoga and integrated fitness experiences in various styles and levels. You can count on our teachers to lead safe, compassionate classes.</div>
                                                        <a data-promo-tag="" id="bg_img_w_cta_link" data-promo-name="Campaign X Name" data-position="Campaign X Location" href="https://www.SSY.com/classes/studio/" title="SEARCH CLASSES" class="btn btn-green">
                                SEARCH CLASSES                            </a>
                                                    </div>
                    </div>
                                            <div class="one-half text-left" style="height: 631.5px;">
                        <img src="<?php echo $theme; ?>assets/Homepage_find-a-class-circle.jpg" alt="Find A Class" class="img-block-1">
                    </div>
                                    </div>
            </div>
        </section>
                <section id="id_1785280494" class="simple-blk block-1" data-set-height="">
            <div class="container-fluid">
                <div class="row">
                                                <div class="one-half second-order" style="height: 526.25px;">
                                <div class="inner">
                                    	                                                                        <h2 class="title-1">Need Flexibility?<br>Get Our App</h2>
	                                                                    <div class="copy"><p>Search and sign up for classes, add favorites to your calendar, schedule reminders and more.</p>
<div class="button-group" style="display: inline-block!important;">
  <a href="https://itunes.apple.com/tw/app/SSY-studio/id933303518?mt=8#" title="Available on the Apple Store"><br>
    <img src="<?php echo $theme; ?>assets/apple_badge.png" width="130" alt="Apple Store Logo Link to SSY App"><br>
  </a><br>
  <a href="https://play.google.com/store/apps/details?id=com.fitnessmobileapps.SSY" title="Available on the Google Play"><br>
    <img src="<?php echo $theme; ?>assets/en_app_rgb_wo_45.png" width="130" alt="Google Store Logo Link to Google Play App"><br>
  </a>
</div>
</div>
                                </div>
                            </div>
                                                        <div class="one-half text-left" style="height: 526.25px;">
                                <img src="<?php echo $theme; ?>assets/yw-mobile-app.png" alt="SSY Mobile App" class="img-block-1">
                            </div>
                                        </div>
            </div>
        </section>
        </div></div></div>
