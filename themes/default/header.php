<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <title><?php echo (cs_var('node') != 'index' ? cs_var('node') . ' - ' : '') . cs_var('name');?></title>
    <link rel="icon" href="<?php echo cs_var('url'); ?>icon-<?php echo cs_var('safeName'); ?>.png" sizes="192x192" />
<?php if (cs_var('rich_app')) include "_head.php"; ?>
<?php seo_tags(); ?>
  </head>
  <body class="site-<?php echo cs_var('safeName'); ?>">
<?php if (cs_var('rich_app')) {?>
<header id="header">
    <div class="container">
      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <h1 class="text-light"><a href="<?php echo cs_var('url');?>"><?php if (cs_var('logo')) echo sprintf('<img src="%slogo-%s.png" alt="%s" class="img-fluid" /> ', cs_var('url'), cs_var('safeName'), cs_var('safeName')); ?><span><?php echo cs_var('name'); ?></span><?php if (cs_var('byline')) echo sprintf(', <span class="x-small">%s</span>', cs_var('byline')); ?></a><?php include_once '_spirit.php' ?></h1>
        
      </div>

      <nav class="main-nav float-right">
        <?php menu(); ?>
      </nav><!-- .main-nav -->
      
    </div>
  </header>
<main id="main" style="margin-top: 100px">
  <section>
    <div class="container">
<?php } else {?>
<div id="nav"><?php menu(); ?></div><?php } ?>