<footer id="footer" class="section-bg">
    <div class="footer-top">
      <div class="container">

        <div class="row">

          <div class="col-lg-6">

            <div class="row">

                <div class="col-sm-6">

                  <div class="footer-info"><?php if (cs_var('logo')) echo sprintf('<a href="./"><img src="%slogo-%s.png" alt="%s" class="img-fluid" /></a><br /><br />', cs_var('url'), cs_var('safeName'), cs_var('safeName')); ?>
                    <h3><?php echo $title; ?></h3>
                    <p><?php echo $description; ?></p>
                  </div>

                </div>

                <div class="col-sm-6">
<?php if (isset($links)) { ?>
                  <div class="footer-links">
                    <h4>Links</h4>
                    <ul><?php foreach ($links as $text => $url) { ?>
                      <li><a href="<?php echo $url; ?>"><?php echo $text; ?></a></li><?php } ?>
                    </ul>
                  </div><?php } ?>

<?php if (isset($address)) { ?>
                  <div class="footer-links">
                    <h4>Contact Us</h4>
                    <p><?php echo $address; ?><br /><br />
                      <strong>Phone:</strong> <a href="tel://<?php echo $phone; ?>"><?php echo $phone; ?></a><br>
                      <strong>WhatsApp:</strong> <a href="https://wa.me/<?php echo $whatsapp; ?>" target="_blank"><?php echo $whatsapp; ?></a><br>
                      <strong>Email:</strong> <a href="mailto:<?php echo $email . (isset($email_subject) ? $email_subject : ''); ?>"><?php echo $email; ?></a><br>
                    </p>
                  </div><?php } ?>

                </div>

            </div>

          </div>

          <div class="col-lg-6">

<?php if (isset($video)) { ?>
                  <div class="video-container"><iframe src="https://www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div><?php } ?>

<?php if (isset($right_widget)) echo $right_widget; ?>

<?php if (isset($social)) { ?>
                  <div class="social-links">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                  </div><?php } ?>

<?php if (isset($facebook_widget)) facebook_widget($facebook_widget); ?>

<?php if (isset($newsletter)) { ?>
                  <div class="footer-newsletter">
                    <h4>Our Newsletter</h4>
                    <p><?php echo $newsletter; ?></p>
                    <form action="<?php echo $newsletter_signup_url; ?>" method="get" target="_blank">
                       <input type="email" name="email"><input type="submit"  value="Subscribe">
                    </form>
                  </div><?php } ?>

<?php if (isset($contact_message)) { ?>
            <div class="form">
              <h4>Send us a message</h4>
              <p><?php echo $contact_message; ?></p>
              <form action="<?php echo $contact_url; ?>" method="post" class="contactForm" target="_blank">
                <div class="form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validation"></div>
                </div>

                <div id="sendmessage">Your message has been sent. Thank you!</div>
                <div id="errormessage"></div>

                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div><?php } ?>

          </div>

          

        </div>

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><?php echo $title; ?></strong> <?php echo (isset($start_year) ? $start_year . ' - ' : '') . date('Y'); ?> All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
        -->
        Designed by <a href="https://bootstrapmade.com/" target="_blank">BootstrapMade</a>
      </div>
    </div>
  </footer>
