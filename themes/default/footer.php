<?php if (cs_var('rich_app')) {?>
    </div>
  </section>
</main>
<?php }
function rich_footer() {
	$extract = cs_var('rich_footer');
	if (!$extract) return false;
	extract($extract);
	include_once '_rich-footer.php';
	return true;
}

if (!rich_footer()) {?>
<hr />Copyright <?php echo date('Y') . ' - ' . cs_var('safeName'); }?>
    <?php if (cs_var('rich_app')) include "_foot.php"; ?>
  </body>
</html>
