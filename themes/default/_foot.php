	<script src="<?php echo $theme; ?>lib/jquery/jquery.min.js"></script>
	<script src="<?php echo $theme; ?>lib/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo $theme; ?>js/main.js"></script>
<?php
if (cs_var('scripts')) foreach (cs_var('scripts') as $file)
  echo sprintf('    <script src="%sassets/%s.js" type="text/javascript"></script>
', cs_var('url'), $file);
?>