
  <!-- Footer -->
  <footer class="py-5 bg-black">
    <div class="container">
      <p class="m-0 text-center text-white small">&copy; Copyright <strong><span><?php echo cs_var('name'); ?></span></strong>. <?php echo (cs_var('start_year') ? cs_var('start_year') . ' - ' : '') . date('Y'); ?> All Rights Reserved
</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo $theme;?>vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo $theme;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Light Slider Image Carousel -->
  <script src="<?php echo $theme;?>vendor/lightslider/js/lightslider.js"></script> 
  <script src="<?php echo $theme;?>vendor/lightslider/lightslider-app.js"></script> 
  <link rel="stylesheet"  href="<?php echo $theme;?>vendor/lightslider/css/lightslider.css"/>
  <link rel="stylesheet"  href="<?php echo $theme;?>vendor/lightslider/lightslider-app.css"/>
  <?php
if (cs_var('styles')) foreach (cs_var('styles') as $file)
	echo sprintf(PHP_EOL . '    <link href="%sassets/%s.css" rel="stylesheet"> ', cs_var('url'), $file);
if (cs_var('scripts')) foreach (cs_var('scripts') as $file)
	echo sprintf(PHP_EOL . '    <script src="%sassets/%s.js" type="text/javascript"></script>', cs_var('url'), $file);
?>
  
</body>

</html>


