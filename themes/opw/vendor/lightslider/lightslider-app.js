$(document).ready(function() {
	$("#content-slider").lightSlider({
		loop:true,
		keyPress:true
	});
	if(window.innerHeight > window.innerWidth)
		$('#image-gallery img').each(function() {
			$(this).attr('src', $(this).attr('src').replace('slides', 'slides_portrait')); //TODO: use from data-folder to data-portrait-folder
		});
	$('#image-gallery').lightSlider({
		//gallery:true,
		item:1,
		thumbItem:10,
		slideMargin:0,
		speed:500,
		auto:true,
		loop:true,
		//pauseOnHover: true,
		onSliderLoad: function() {
			$('#image-gallery').removeClass('cS-hidden');
		}
	});
});