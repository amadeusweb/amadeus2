<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?php echo (cs_var('node') != 'index' ? cs_var('node') . ' - ' : '') . cs_var('name') . (cs_var('node') == 'index' ? ' | ' . cs_var('byline') : '');?></title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo $theme;?>assets/img/favicon.png" rel="icon">
  <link href="<?php echo $theme;?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo $theme;?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo $theme;?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo $theme;?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo $theme;?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo $theme;?>assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo $theme;?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo $theme;?>assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo $theme;?>assets/css/style.css" rel="stylesheet">
<?php
if (cs_var('head_hooks')) foreach (cs_var('head_hooks') as $hook) include_once $hook;
?>
  <!-- =======================================================
  * Template Name: OnePage - v2.1.0
  * Template URL: https://bootstrapmade.com/onepage-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <a href="<?php echo cs_var('url');?>"><img src="<?php echo cs_var('url'); ?>logo-<?php echo cs_var('safeName') . cs_var('safeFolder'); ?>.png" alt="<?php echo cs_var('name'); ?>" class="img-fluid"></a>
      <?php if (!cs_var('no_name_link_in_header')) {?><h1 class="logo mr-auto"><a href="<?php echo cs_var('url'); ?>"><?php echo cs_var('name'); ?></a></h1><?php }?>

      <nav class="nav-menu d-none d-lg-block">
        <?php if (file_exists(cs_var('path') . '/_menu.php')) include_once cs_var('path') . '/_menu.php'; else menu(); ?>
      </nav><!-- .nav-menu -->

      <a href="<?php echo cs_var('contact_cta_link'); ?>" class="get-started-btn scrollto"><?php echo cs_var('contact_cta_text'); ?></a>

    </div>
  </header><!-- End Header -->