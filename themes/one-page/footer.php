  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <?php if (file_exists(cs_var('path') . '/_footer-content.php')) include_once cs_var('path') . '/_footer-content.php'; else { ?>
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>OnePage</h3>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br><br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> info@example.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>
    <?php } ?>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span><?php echo cs_var('name'); ?></span></strong>. <?php echo (cs_var('start_year') ? cs_var('start_year') . ' - ' : '') . date('Y'); ?> All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/onepage-multipurpose-bootstrap-template/ -->
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
     </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="<?php echo $theme; ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo $theme; ?>assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo $theme; ?>assets/js/main.js"></script>
  <?php
  if (cs_var('styles')) foreach (cs_var('styles') as $file)
    echo sprintf(PHP_EOL . '    <link href="%sassets/%s.css" rel="stylesheet"> ', cs_var('url'), $file);
  if (cs_var('scripts')) foreach (cs_var('scripts') as $file)
    echo sprintf(PHP_EOL . '    <script src="%sassets/%s.js" type="text/javascript"></script>', cs_var('url'), $file);
  ?>

  </body>

  </html>