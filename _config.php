<?php
$local = $_SERVER['HTTP_HOST'] ==='localhost';
return [
	'app' => $local ? 'http://localhost/cs/doms/cselian-cms/' : 'https://cms.cselian.com/',
	'logo' => 'logo-cselian.jpg',
	'rich_app' => true,
	'rich_footer' => [
		'title' => 'CSELIAN',
		'description' => 'A Tech Company providing great results!!!',
		//'newsletter' => 'We encourage Clients, Developers and Entrepreneurs to signup for news and tips',
		//'newsletter_signup_url' => 'https://facebook.com/cselian/',
		'links' => [
			//TODO: remove target blank as sites incorporate our footer
			'Cselian Home' => '//cselian.com/',

			'Amadeus CMS' => '//cms.cselian.com/',
			'My KNK' => '//myknk.in/',
			//'Teach Me Yoga' => '//teachmeyoga.in/',
			'Dyners*' => '//eat.cselian.com/" target="_blank',
		],
		'address' => 'Flat No 6, Rams Apartments,<br /> 5/2A Sir Madhavan Nair Road,<br /> Mahalingapuram,<br /> Chennai 600034',
		'phone' => '+919841223313', 'whatsapp' => '+919841223313',
		'email' => 'services@cselian.com',
		'email_subject' => '?subject=Cselian+Business+Enquiry',
		//'contact_message' => 'Get in touch with us for any sort of Website / Online Marketing / Architecture / Entrepreneurial need.',
		//'contact_url' => 'https://cselian.com/mail/',
		'facebook_widget' => 'cselian',
		'start_year' => 2005,
	],
];
?>
