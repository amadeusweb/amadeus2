<?php
function tsv_to_array($data, &$cols = null) {
	$r = array();
	$lines = explode("
", $data);
	foreach ($lines as $lin)
	{
		//$lin = trim($lin, '\r\n');
		if ($lin == '' || $lin[0] == '#')
		{
			if ($lin != '')
				if ($cols == 'object') tsv_set_cols($lin, $cols); else $cols = explode("	", substr($lin, 1));
			continue;
		}
		$r[] = explode("	", $lin);
	}
	return $r;
}

function tsv_set_cols($lin, &$c)
{
	$lin = substr($lin, 1);
	$r = explode("	", $lin);
	$c = new stdClass();
	foreach ($r as $key => $value)
	{
		$value = trim($value);
		$c->$value = trim($key);
	}
}

function tsv_parse_cols($colsRaw, &$colsByName) {
	$cols = [];
	foreach ($colsRaw as $col => $ix) {
		$bits = explode(':', $col);

		$c = new stdClass();
		$c->name = $bits[0];
		$c->index = $ix;
		$c->type = isset($bits[1]) ? $bits[1] : 'text';

		$cols[] = $c;
		$colsByName[$c->name] = $c;
	}
	return $cols;
}

function render_tsv($raw, array $filter) {
	$colsRaw = true;
	$tsv = tsv_to_array($raw, $colsRaw);
	$r = '<table border="1" cellpadding="4"><tr>';

	$colsByName = [];
	$cols = tsv_parse_cols($colsRaw, $colsByName);

	$skip = cs_var('skip_fields');
	foreach ($cols as $c) {
		if ($skip && array_search($c->name, $skip) !== false) continue;
		$r .= sprintf('<th title="%s">%s</th>' . PHP_EOL, $c->type, $c->name);
	}

	$loc = isset($colsByName['Location']) ? $colsByName['Location'] : false;
	$cat = isset($colsByName['Category']) ? $colsByName['Category'] : false;
	foreach ($tsv as $row)
	{
		if ($loc && isset($filter['Location']) && $row[$loc->index] != $filter['Location']) continue;
		if ($cat && isset($filter['Category']) && $row[$cat->index] != $filter['Category']) continue;
	
		$r .= '<tr>';
		if (substr($row[0], 0, 1) == '|') {
			$r .= '<td class="merged-cell" colspan="' . count($cols) . '">' . substr($row[0], 1) . '</td>';
		} else {
			foreach ($cols as $c) {
				if ($skip && array_search($c->name, $skip) !== false) continue;
				$r .= '<td>' . (isset($row[$c->index]) ? render_cell($row[$c->index], $c->type, $c) : '-'). '</td>' . PHP_EOL;
			}
		}

		$r .= '<tr>';
	}

	$r .= '</table>';

	return $r;
}

function render_cell($raw, $type, $col = false) {
	if ($raw == '') return $raw;

	if ($type == 'list') {
		$ends = ['<li>', '</li> '];
		return '<ul class="list">' . $ends[0] . implode($ends[1] . $ends[0], explode(', ', $raw)) . $ends[1] . '</ul>';
	}

	if ($type == 'url') {
		$bits = explode('|', $raw);
		$cls = [];
		if ($col) $cls[] = strtolower($col->name);
		if (stripos($bits[0], 'youtube.com') != -1) $cls[] = 'youtube';
		$cls = count($cls) ? ' class="' . implode(' ', $cls) . '"' : '';
		return sprintf('<a%s href="%s" target="_blank">%s</a>', $cls, $bits[0], isset($bits[1]) ? $bits[1] : $bits[0]);
	}

	if ($type == 'img') {
		return sprintf('<img src="%s%s" width="400" class="img-fluid" />' . PHP_EOL, cs_var('imgUrl'), $raw);
	}

	if ($col->name == 'Page') {
		return sprintf('<a href="%s">%s</a>' . PHP_EOL, $raw, $raw);
	}

	return $raw;
}

?>