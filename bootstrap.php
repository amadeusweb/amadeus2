<?php
$nl = "\r\n"; $br = '<br/>';
cs_var('nl', $nl);
cs_var('br', $br);
cs_var('brnl', $br . $nl);

function urlize($txt) {
	return str_replace(' ', '-', strtolower($txt));
}

function cs_var($name, $val = null)
{
	global $cscore;
	if (!isset($cscore)) $cscore = array();
	if ($val != null)
		$cscore[$name] = $val;
	else
		return isset($cscore[$name]) ? $cscore[$name] : false;
}

function cs_sub_var($parent, $key)
{
	$a = cs_var($parent);
	return is_array($a) ? $a[$key] : false;
}

function cs_vars($a)
{
	foreach ($a as $key=>$value)
		cs_var($key, $value);
}

function bootstrap($config) {
	cs_vars($config);
	cs_var('app-fol', __DIR__ . '/');
	
	$node = isset($_GET['node']) && $_GET['node'] ? $_GET['node'] : 'index';
	if (substr($node, -1) == '/') $node = substr($node, 0, strlen($node) - 1); //TODO: Redirect / canonical
	cs_var('node', $node);
}

function menu() {
	$menu = cs_var('path') . '/menu.tsv';
	$base = cs_var('url');

	if (file_exists($menu)) {
		$cols = 'object';
		$menu = tsv_to_array(file_get_contents($menu), $cols);

		echo '<ul>'; $first = true; $last = false;

		if (isset($cols->Parent)) {
			foreach($menu as $item) {
				$file = $item[$cols->Name];
				$parent = $item[$cols->Parent] == 'Y';

				if ($parent && !$first) echo '</ul>';

				echo sprintf('  <li%s><a href="./%s">%s</a>' . PHP_EOL, $parent ? ' class="drop-down"' : '' , str_replace('index', '', $file), str_replace('index', 'home', $file));
				if ($parent) {
					echo '<ul>';
				} else {
					echo '</li>';
				}
				$first = false;
			}
			echo '</ul></li></ul>';
			return;
		} else if (isset($cols->Level) && isset($cols->HasSubmenu)) {
			foreach($menu as $item) {
				if ($item[$cols->Level] == '1' && !$first) { echo '</ul></li>'; $first = true; }

				$link = $item[$cols->Page];
				if ($link !== '' && $link[0] == '*')
					$link = substr($link, 1) . '" target="_blank';
				else
					$link = $base . $link . ($link == '' ? '' : '/');

				if ($item[$cols->Level] == 2 || ($item[$cols->Level] == 1 && $item[$cols->HasSubmenu] == ''))
					echo sprintf('  <li><a href="%s">%s</a></li>' . PHP_EOL, $link, $item[$cols->Name]);

				if ($item[$cols->Level] == 1 && $item[$cols->HasSubmenu] == 'Y') {
					echo sprintf('  <li class="drop-down"><a href="%s">%s</a><ul>' . PHP_EOL, $link, $item[$cols->Name]);
					$first = false;
				}
			}
			echo ($item[$cols->Level] == 2 ? '</li></ul>' : '') . '</ul>';
			return;
		}
	}

	echo '<ul>';
	$files = scandir(cs_var('path'));

	$home = 'index.txt';
	unset($files[array_search($home, $files)]);
	$files = array_merge([$home], $files);

	foreach ($files as $file) {
		if (strpos($file, '.txt') === false && strpos($file, '.php') === false) continue;
		if ($file == 'index.php' || $file == 'index.cms.php' || substr($file, 0, 1) == '_') continue;
		$file = str_replace('.php', '', str_replace('.cms.php', '', str_replace('.txt', '', $file)));
		if ($file == 'bootstrap' || $file == 'parent_config' || $file == 'autoloader') continue;
		if ($file == cs_var('folderName')) continue;
		echo sprintf('  <li><a href="%s/">%s</a></li>' . PHP_EOL, $base . str_replace('index', cs_var('folder') ? cs_var('folderName') : '', $file), str_replace('index', 'home', $file));
	}
	echo '</ul>';
}

include '_wp-functions.php';
include '_array-functions.php';
include '_page-functions.php';
include '_social-functions.php';

function render() {
	if (function_exists('before_render')) before_render();

	$themeName = cs_var('theme') ? cs_var('theme') : 'default';
	$theme = cs_var('app') . "themes/$themeName/";
	cs_var('theme_url', $theme);
	include_once "themes/$themeName/header.php";

	$file =  cs_var('path') . '/' . cs_var('node') . '.txt';

	if (!file_exists($file)) $file =  cs_var('path') . '/' . cs_var('node') . '.cms.php';
	if (!file_exists($file)) $file =  cs_var('path') . '/' . cs_var('node') . '.php';
	if (file_exists($file) && substr($file, -4) == '.php') {
		include_once $file;
		include_once "themes/$themeName/footer.php";
		return;
	}

	if (!file_exists($file)) {
		if (function_exists('did_render_page') && did_render_page()) {
			//noop
		} else {
			echo '<h2 style="color: red">Could NOT find file ' . cs_var('node') . '</h2>';
			//NB: create mode $file =  cs_var('path') . '/' . cs_var('node') . '.txt'; file_put_contents($file, sprintf('//TODO: create %s file', cs_var('node')));
		}
		include_once "themes/$themeName/footer.php";
		return;
	}

	$raw = file_get_contents($file);
	$raw = str_replace('[theme]', $theme, $raw);

	if (stripos($raw, '[details]') !== false)
		$raw = str_replace('[details]', file_get_contents(cs_var('path') . '/_details.txt') , $raw);
	if (stripos($raw, '[menu]') !== false)
		$raw = str_replace('[menu]', fn_menu(), $raw);
	if (stripos($raw, '[order]') !== false)
		$raw = str_replace('[order]', fn_order(), $raw);

	echo wpautop($raw);

	include_once "themes/$themeName/footer.php";
}

function seo_tags() {
	$files = [cs_var('dataFile'), cs_var('path') . '/pages.tsv', cs_var('path') . '/menu.tsv'];
	$page = cs_var('node');
	$found = false;

	foreach ($files as $file) {
		if (!file_exists($file)) continue;

		$cols = 'object';
		$seo = tsv_to_array(file_get_contents($file), $cols);
		if (!isset($cols->Description)) continue;

		foreach ($seo as $pg) {
			if ($pg[$cols->Page] != $page) continue;
			$fmt = '    <meta name="%s" content="%s" />' . PHP_EOL;
			echo sprintf($fmt, 'description', $pg[$cols->Description]);
			echo sprintf($fmt, 'keywords', $pg[$cols->Keywords]);
			$found = true;
			break;
		}
		if ($found) break;
	}
}
?>
