<?php

function social_fb_load_once() {
	if (cs_var('social_fb_script_loaded')) return; ?>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0&appId=1613378742233452&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php
	cs_var('social_fb_script_loaded', true);
}

cs_vars([
	'nl' => '
',
	'social_fb_like' => '<div class="fb-like" style="display: block; margin-bottom: 8px;" data-href="https://www.facebook.com/%s" data-send="true" data-layout="button_count" data-width="450" data-show-faces="true"></div>',
	'social_fb_page' => '<div class="fb-page" data-href="https://www.facebook.com/%s" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/%s" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/%s">%s</a></blockquote></div>',
]);

function facebook_widget($vars) {
	$nl = cs_var('nl');
	$name = is_string($vars) ? $vars : $vars['name'];
	social_fb_load_once();
	echo $nl . sprintf(cs_var('social_fb_like'), $name);
	echo $nl . sprintf(cs_var('social_fb_page'), $name, $name, $name, $name) . $nl;
}
?>
