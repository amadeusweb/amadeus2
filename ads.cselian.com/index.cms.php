This ad platform is powering:
<ol>
<?php
$cols = 'object';
$rows = tsv_to_array(
'#Link	Notes	Asterisk
pin.yieldmore.org	Public Interest Network (of NGOs, Educators & Healers)	*
ads.cselian.com/yoga/	Yoga Instructors	
ads.cselian.com/knk/	Restaurants and Places in KNK, Chennai	
findplacesinindia.com/properties/	Real Estate Properties	*
offersgalore.in	What offers are on in Nearby Supermarkets	*
', $cols);

foreach ($rows as $r)
	echo sprintf('  <li><a href="https://%s" target="_blank">%s</a> %s - %s</li>', $r[$cols->Link], $r[$cols->Link], $r[$cols->Asterisk], $r[$cols->Notes]);
?>
</ol>
* - These websites are yet to be built.
