<?php
include_once '../../bootstrap.php';
include_once '../autoloader.php';
$local = $_SERVER['HTTP_HOST'] ==='localhost';
bootstrap(array_merge(include_once('../../_config.php'), array(
	'name' => 'My KNK',
	'spirit' => 'i=integrated',
	'safeName' => 'my-knk',
	'byline' => 'Community at Khader Nawaz Khan Road',
	'url' => $local ? 'http://localhost/cs/doms/cselian-one/ads.cselian.com/knk/' : 'https://myknk.in/',
	'path' => __DIR__,
	'dataFile' => dirname(__DIR__) . '/knk/ads.tsv',
	'rich_footer' => [
		'title' => 'My KNK',
		'description' => 'A Goodwill Community Initiative for Residents and Businesses at Khader Nawaz Khan Road',
		'links' => [
			'Ads Galore' => 'https://ads.cselian.com/',
			'Team' => './team',
		],
	],
)));
render();
?>
