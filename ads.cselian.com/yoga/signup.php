<style type="text/css">
th { text-align: right; }
th, td { padding: 6px; }
</style>
<form action="./signup" method="post">
	<table>
		<tr><th>Number</th>
			<td><input type="text" name="phone" /></td></tr>
		<tr><th>Missed Call</th>
			<td><a href="tel:+918374175976">Sundaram</a> / <a href="https://wa.me/+918374175976" target="_blank">WhatsApp</a></td></tr>
		<tr><th><a href="./terms" target="blank">Terms</a></th>
			<td><label><input type="checkbox" id="agree" onchange="toggleAgree()" value="agree" /> I agree</label></td></tr>
		<tr><th>Next Step</th>
			<td><label><input type="submit" id="build" value="Build My Site" disabled /></td></tr>
	</table>
</form>
<script type="text/javascript">
function toggleAgree() {
	var a = document.getElementById('agree');
	var b = document.getElementById('build');
	if (a.checked) b.removeAttribute("disabled"); else b.setAttribute("disabled", true);
}
</script>