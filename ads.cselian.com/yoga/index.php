<?php
include_once '../../bootstrap.php';
include_once '../autoloader.php';
$local = $_SERVER['HTTP_HOST'] ==='localhost';
bootstrap(array_merge(include_once('../../_config.php') , array(
	'name' => 'TeachMeYoga.in',
	'safeName' => 'teach-me-yoga',
	'byline' => 'Find Instructors',
	'url' => $local ? 'http://localhost/cs/doms/cselian-one/ads.cselian.com/yoga/' : 'https://ads.cselian.com/yoga/',
	'path' => __DIR__,
	'dataFile' => dirname(__DIR__) . '/yoga/ads.tsv',
)));
render();
?>
