<?php
return [
	'network'=> 'yoga',
	'imgUrl' => cs_var('url'),
	'dataFile' => dirname(__DIR__) . '/yoga/ads.tsv',
	'premiumFields' => explode('	', 'Latitude	Longitude	Website	Number	Email'),
];
?>