<?php
function pg_ads($cfg) {
	//print_r($cfg);
	if (isset($cfg['showMeta'])) { echo '//TODO: META'; return; }

	cs_var('imgUrl', $cfg['imgUrl']);

	$skip = ['Text', 'Description', 'Keywords', 'Category', 'Website'];
	if (isset($cfg['premiumFields'])) $skip = array_merge($skip, $cfg['premiumFields']);
	cs_var('skip_fields', $skip);

	$raw = file_get_contents($cfg['dataFile']);

	$filter = [];

	if (isset($cfg['location'])) { echo 'LOCATION: ' . $cfg['location']; $filter['Location'] = $cfg['location']; }
	if (isset($cfg['category'])) { echo 'CATEGORY: ' . $cfg['category']; $filter['Category'] = $cfg['category']; }

	echo render_tsv($raw, $filter);
}

function did_render_page() {
	$raw = file_get_contents(cs_var('dataFile'));
	//pg_ads(include_once('_config.php'));
	
	$page = cs_var('node');
	$colsRaw = true;
	$tsv = tsv_to_array($raw, $colsRaw);
	$colsByName = [];
	$cols = tsv_parse_cols($colsRaw, $colsByName);

	$skip = ['Description', 'Keywords'];
	foreach ($tsv as $r) {
		if ($r[$colsByName['Page']->index] != $page) continue;
		echo '<table border="1" cellpadding="4">' . PHP_EOL;
		foreach ($cols as $c) {
			if ($skip && array_search($c->name, $skip) !== false) continue;
			echo '<tr><th>' . $c->name . '</th>';
			echo '<td>' . (isset($r[$c->index]) ? render_cell($r[$c->index], $c->type, $c) : '-'). '</td></tr>' . PHP_EOL;
		}
		echo '</table>';
		return true;
	}
	return false;
}
?>
