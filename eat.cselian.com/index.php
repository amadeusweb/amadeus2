<?php
include_once '../bootstrap.php';
bootstrap(array_merge(include_once('../_config.php') , array(
	'safeName' => 'Restarateurs @ CS',
	'byline' => 'Food To fit your lifestyle & health.',
	'url' => $local ? 'http://localhost/cs/doms/cselian-one/eat.cselian.com/' : 'https://eat.cselian.com/',
	'nextline' => 'Specialized in Anglo Indian Cuisine!!',
	'address' => 'Chennai,<br/> India',
	'facebook' => 'https://www.facebook.com/DestinysFood/',
	'theme' => 'delicious',
	'name' => 'eat.cselian.com',
	'path' => __DIR__,
	'menu_item' => '<div class="menu-%Category% menu-restaurant">
            <span class="clearfix">
              <a class="menu-title" href="#">%Name%</a>
              <span style="left: 166px; right: 44px;" class="menu-line"></span>
              <span class="menu-price">Rs %Price%</span>
            </span>
            <span class="menu-subtitle">%Description%</span>
          </div>',
)));
render();
?>
