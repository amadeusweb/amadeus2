<?php
function fn_id()
{
	$file = cs_var('path') . '/protected/orders/last-id.txt';
	$i = file_exists($file) ? intval( file_get_contents($file) ) : 0;
	$i++;
	file_put_contents($file, $i . '');
	return $i;
}

function fn_otp() {
	return rand(9999, 1000);
}

function fn_hash($i, $otp) {
	return str_replace('=', '', $i . strrev(base64_encode( $otp + $i))); //TODO: use better hashing
}

function fn_menu()
{
	$file = cs_var('path') . '/protected/menu.tsv';
	$cols = 'object';
	$items = tsv_to_array( file_get_contents($file), $cols);

	$r = '';
	$fmt = cs_var('menu_item');

	foreach ($items as $item) {
		$vars = ['Name' => $item[$cols->Name], 'Category' => $item[$cols->Category], 'Description' => $item[$cols->Description], 'Price' => $item[$cols->Price]];
		$itm = $fmt;
		foreach ($vars as $key=>$var)
			$itm = str_replace('%' . $key . '%', $var, $itm);
		$r .= $itm . PHP_EOL . PHP_EOL;
	}
	return $r;
}

function fn_order()
{
	$url = $_SERVER['REQUEST_URI'];
	$bits = explode('?id=', $url);
	if (count($bits) == 2) {
		$id = $bits[1];
		$file = cs_var('path') . '/protected/orders/order-'.$id.'.txt';
		$r = file_get_contents($file);
	} else if (isset($_POST['name'])) {
		$otp = fn_otp();
		$id = fn_hash(fn_id(), $otp);
		$file = cs_var('path') . '/protected/orders/order-'.$id.'.txt';

		$fields = ['Name', 'Email', 'Phone', 'Address', 'Pincode', 'Instructions'];
		$r = '<!--more--><!--customer-details-->' . PHP_EOL;

		foreach ($fields as $f)
			$r .= $f . ': ' . $_POST[strtolower($f)] . PHP_EOL;
		$r .= 'OTP: ' . $otp;

		$r .= '<!--more--><!--order-items-->' . PHP_EOL;
		$r .= '<!--more--><!--total-->' . PHP_EOL;

		file_put_contents($file, $r);
		//mail('services@cselian.com', cs_var('safeName') . ' order - ' . $id, $r);
	} else {
		echo 'Order not submitted or url incorrect';
		return;
	}
	echo 'Order <a href="' . cs_var('url') . 'order?id=' . $id . '">#' . $id . '</a><br />' . PHP_EOL;
	echo '<textarea style="height: 50vh; width: 90vw;">' . $r . '</textarea>';
}

?>