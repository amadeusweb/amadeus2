This micro cms framework is powering:

<ol>
<?php
$cols = 'object';
$rows = tsv_to_array(
'#Link	Notes
ads.cselian.com	Ad System being built (will feature a wordpress plugin for consumption)
eat.cselian.com	Order System being built (will have POS Integration, Aggregator Collation and BI Reports)
bhuvanaseshan.in	To be moved from wordpress
destinyskitchen.in	To be registered
hopeandlove.in	An NGO in calcutta (theme pending)
mini.cselian.com	To be moved once a teammate is onboarded c 2020
stardustlights.in	A designer in Bangalore. Waiting to confirm domain
vibhasethi.in	Theming pending
zeroviolation.com	Theming pending'
, $cols);

foreach ($rows as $r)
	echo sprintf('  <li><a href="https://%s" target="_blank">%s</a> - %s</li>', $r[$cols->Link], $r[$cols->Link], $r[$cols->Notes]);


?>
</ol>