<?php
include_once 'bootstrap.php';
bootstrap(array_merge(include_once('_config.php') , array(
	'safeName' => 'amadeus',
	'name' => 'Amadeus CMS',
	'path' => __DIR__,
)));
render();
?>
